= Title component 1.0 ROOT topicA/page5.adoc
:description: Page A5
:page-name: page5
:odd:

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}

description: {description}

== A page

And some content.
