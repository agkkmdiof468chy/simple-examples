= Gneiss
:tag: mineral
:description: Gneiss is a high-grade metamorphic rock with distinct foliation.

{description}

== Related Pages

indexDescriptionList::[attributes='tag=mineral',descAttribute=description,style=horizontal]
