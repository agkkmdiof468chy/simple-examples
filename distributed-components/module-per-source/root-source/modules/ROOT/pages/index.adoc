= {page-component-title}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}


== Organizing a distributed component

A distributed component is assembled from any number of sources where the antora.yml component descriptors share the same component name and version.
As long as you avoid name collisions in the content, any organization is possible.
A convenient layout is to put each module into one source; a source may contain several modules, but all source for a given module is entirely within a single source.
This project illustrates this layout choice.

* module1-source contains module1.
* module2-3-source contains module2 and module3.
* root-source contains ROOT.

This page is from root-source.
